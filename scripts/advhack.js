server = "iron-gym";

while (!hasRootAccess(server)) {
  hack("foodnstuff");
  grow("foodnstuff");
  weaken("foodnstuff");
}

while (true) {
  while (getServerMaxMoney(server) * 0.1 >= getServerMoneyAvailable(server)) {
    if (
      getServerSecurityLevel(server) / 1.5 >=
      getServerMinSecurityLevel(server)
    ) {
      weaken(server);
    }
    grow(server);
  }

  hack(server);
}
