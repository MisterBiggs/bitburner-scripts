function myMoney() {
  return getServerMoneyAvailable("home");
}

disableLog("getServerMoneyAvailable");
disableLog("sleep");

threads = Math.max(
  1,
  Math.floor(
    (getServerMaxRam("home") - getServerUsedRam("home")) /
      getScriptRam("advhack.script")
  )
);
run("advhack.script", threads);

var cnt = 8;

while (hacknet.numNodes() < cnt) {
  res = hacknet.purchaseNode();
  print("Purchased hacknet Node with index " + res);
}

for (var i = 0; i < cnt; i++) {
  while (hacknet.getNodeStats(i).level <= 80) {
    var cost = hacknet.getLevelUpgradeCost(i, 10);
    while (myMoney() < cost) {
      print("Need $" + cost + " . Have $" + myMoney());
      sleep(3000);
    }
    res = hacknet.upgradeLevel(i, 10);
  }
}

tprint("All nodes upgraded to level 80");

for (var i = 0; i < cnt; i++) {
  while (hacknet.getNodeStats(i).ram < 16) {
    var cost = hacknet.getRamUpgradeCost(i, 2);
    while (myMoney() < cost) {
      print("Need $" + cost + " . Have $" + myMoney());
      sleep(3000);
    }
    res = hacknet.upgradeRam(i, 2);
  }
}

tprint("All nodes upgraded to 16GB RAM");

for (var i = 0; i < cnt; i++) {
  while (hacknet.getNodeStats(i).cores < 8) {
    var cost = hacknet.getCoreUpgradeCost(i, 1);
    while (myMoney() < cost) {
      print("Need $" + cost + " . Have $" + myMoney());
      sleep(3000);
    }
    res = hacknet.upgradeCore(i, 1);
  }
}

tprint("All nodes upgraded to 8 cores");
tprint("===Hacknet Nodes Complete===");

ram = 1024;
hn = "pserv-";
for (i = 0; i < getPurchasedServerLimit(); ++i) {
  var cost = getPurchasedServerCost(ram);
  while (myMoney() < cost) {
    print("Need $" + cost + " . Have $" + myMoney());
    sleep(30000);
  }
  purchaseServer(hn + i, ram);
  tprint(hn + i, " added to swarm.");
}

scriptKill("advhack.script", "home");
threads = Math.max(
  1,
  Math.floor(
    (getServerMaxRam("home") -
      getServerUsedRam("home") +
      getScriptRam("hn.script")) /
      getScriptRam("advhack.script")
  )
);
spawn("advhack.script", threads);
