script = "advhack.script";
nuke("foodnstuff");
servers = scan();
nuked = ["home"];

i = 0;
while (i < servers.length) {
  if (servers[i] == "darkweb") {
    i++;
    continue;
  }
  while (getHackingLevel() < getServerRequiredHackingLevel(servers[i])) {
    sleep(20000);
  }

  scp(script, servers[i]);
  if (!hasRootAccess(servers[i])) {
    // dont hack purchased servers
    if (getServerNumPortsRequired(servers[i])) {
      while (!fileExists("BruteSSH.exe")) {
        sleep(20000);
      }
      brutessh(servers[i]);
    }
    nuke(servers[i]);
  }

  threads = Math.max(
    1,
    Math.floor(getServerMaxRam(servers[i]) / getScriptRam(script))
  );
  exec(script, servers[i], threads);

  nuked.push(servers[i]);
  tprint(servers[i], " added to botnet with ", threads, " threads");

  ++i;
}

tprint("===Basic Servers Finished===");

while (true) {
  tonuke = [];
  i = 0;
  while (i < nuked.length) {
    n = scan(nuked[i]);
    j = 0;
    while (j < n.length) {
      if (!nuked.includes(n[j])) {
        tonuke.push(n[j]);
      }
      j++;
    }
    i++;
  }

  i = 0;
  while (i < tonuke.length) {
    if (getHackingLevel() < getServerRequiredHackingLevel(tonuke[i])) {
      i++;
      continue;
    }

    if (!hasRootAccess(tonuke[i])) {
      // dont hack purchased servers
      ports = 0;
      if (fileExists("BruteSSH.exe")) {
        brutessh(tonuke[i]);
        ports++;
      }
      if (fileExists("FTPCrack.exe")) {
        ftpcrack(tonuke[i]);
        ports++;
      }
      if (fileExists("relaySMTP.exe")) {
        relaysmtp(tonuke[i]);
        ports++;
      }
      if (fileExists("HTTPWorm.exe")) {
        httpworm(tonuke[i]);
        ports++;
      }
      if (fileExists("SQLInject.exe")) {
        sqlinject(tonuke[i]);
        ports++;
      }
      if (getServerNumPortsRequired(tonuke[i]) > ports) {
        i++;
        continue;
      }
      // ftpcrack(tonuke[i]);
      nuke(tonuke[i]);
    }

    scp(script, tonuke[i]);

    //Execute our scripts on the target server
    threads = Math.max(
      1,
      Math.floor(getServerMaxRam(tonuke[i]) / getScriptRam(script))
    );
    exec(script, tonuke[i], threads);
    nuked.push(tonuke[i]);
    tprint(
      tonuke[i],
      " added to botnet with ",
      threads,
      " threads. Server has $",
      getServerMoneyAvailable(tonuke[i]) / 1e6
    ),
      "M";
    i++;
  }
  sleep(2000);
}
tprint("===== SPIDER FINISHED =====");
